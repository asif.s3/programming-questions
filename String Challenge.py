# String Challenge

def is_valid(st):
    if st[0].isalpha() or st[len(st)-1].isalpha():
        return False

    for i in range(1, len(st)-1):
        if st[i].isalpha() and (st[i-1] != '+' or st[i+1] != '+'):
            return False

    return True


s = input()
print(is_valid(s))

